<?php
namespace App\services;

use App\models\Order;

class DiscountFacade {
         
    private $order, $customers, $products;
    private $services;
     
    public function __construct(array $customers, array $products) {
        $this->customers = $customers;
        $this->products  = $products;

        $this->services  = [
            \App\services\Over1000Service::class,
            \App\services\BuyFiveOneFreeService::class,
            \App\services\MoreThan1Cat1Service::class
        ];
    }

    public function calculateDiscount(Order $order){
        $discounts = [];
        
        foreach ($this->services as $service) {
            $instance = new $service( $this->products, $this->customers);
            $discount = $instance->calculate($order);
            if(!is_null($discount)) {
                $discounts[] = $discount;
            }
        }

        return $discounts;
    }
}