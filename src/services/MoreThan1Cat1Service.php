<?php
namespace App\services;

use App\models\Discount;
use App\models\Order;

class MoreThan1Cat1Service
{
    public function __construct($products){
        $this->products  = $products;
    }

    /*
     * If you buy two or more products of category "Tools" (id 1), you get a 20% discount on the cheapest product.
     */
    public function calculate(Order $order){
       $productIDsCat1 = $this->getProductIDsByCategory(1);

        $itemsFromCat1 = [];
        $itemsTotal    = 0;
        foreach ( $order->getItems() as $item ) {
            if ( in_array($item->getProductId(), $productIDsCat1)) {
                $itemsTotal += $item->getQuantity();
                $itemsFromCat1[] = array(
                    "productId"  => $item->getProductId(), 
                    "quantity"   => $item->getQuantity(), 
                    "unit_price" => $item->getUnitPrice() );

            }
        }

        if( $itemsTotal >= 2 ){
            usort($itemsFromCat1, function($a, $b) {
                return $a['unit_price'] > $b['unit_price'];
            });

            return new Discount("Type 3", $itemsFromCat1[0]['unit_price']*0.20*$itemsFromCat1[0]['quantity'] );
        }
        return;
    }

    private function getProductIDsByCategory(int $cat){
        $productsCat = array_filter( $this->products, function($prod) use($cat){
            return($prod->getCategory() == $cat);
        });

        $productIDsCat = [];
        foreach ($productsCat as $product) {
            $productIDsCat[] = $product->getId();
        }
        return $productIDsCat;
    }
}