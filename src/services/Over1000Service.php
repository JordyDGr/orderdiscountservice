<?php
namespace App\services;

use App\models\Discount;
use App\models\Order;

class Over1000Service
{
    public function __construct(array $products, array $customers){
        $this->products  = $products;
        $this->customers = $customers;
    }

    /*
     * A customer who has already bought for over € 1000, gets a discount of 10% on the whole order.
     */
    public function calculate(Order $order){
        $customer = $this->getCustomerById($order->getCustomerId());

        if($customer != null && $customer->getRevenue() > 1000){
            return new Discount("Type 1" , $order->getTotal()*0.10 );
        }
        return;
    }

    private function getCustomerById(int $id){
        foreach ( $this->customers as $customer ) {
            if ( $customer->getId() === $id) {
                return $customer;
            }
        }
        return;
    }
}