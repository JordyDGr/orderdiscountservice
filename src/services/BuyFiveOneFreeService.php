<?php
namespace App\services;

use App\models\Discount;
use App\models\Order;

class BuyFiveOneFreeService
{
    public function __construct(array $products){
        $this->products  = $products;
    }

    /*
     * For every product of category "Switches" (id 2), when you buy five, you get a sixth for free.
     */
    public function calculate(Order $order){
        $productIDsCat2 = $this->getProductIDsByCategory(2);

        $discount = 0.0;
        foreach ( $order->getItems() as $item ) {
            if ( in_array($item->getProductId(), $productIDsCat2)) {
                $itemsForFree = floor( $item->getQuantity() / 6 );
                $discount    += $itemsForFree * $item->getUnitPrice();
            }
        }

        if($discount > 0) {
            return new Discount("Type 2" , $discount );    
        }
        
        return;
    }

    private function getProductIDsByCategory(int $cat){
        $productsCat = array_filter( $this->products, function($prod) use($cat){
            return($prod->getCategory() == $cat);
        });

        $productIDsCat = [];
        foreach ($productsCat as $product) {
            $productIDsCat[] = $product->getId();
        }
        return $productIDsCat;
    }
}