<?php
namespace App\models;

class Product
{

    private $id, $description, $category, $price;

    public function getId() {
        return $this->id;
    }

    public function setId(string $id) {
        $this->id = $id;
    }

    public function getDescription() {
        return $this->description;
    }

    public function setDescription(string $description) {
        $this->description = $description;
    }

    public function getCategory() {
        return $this->category;
    }

    public function setCategory(int $category) {
        $this->category = $category;
    }

    public function getPrice() {
        return $this->price;
    }

    public function setPrice(float $price) {
        $this->price = abs($price);
    }
}
