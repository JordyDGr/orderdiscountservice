<?php
namespace App\models;

class Item
{

    private $productId, $quantity, $unitPrice, $total;

    public function getProductId() {
        return $this->productId;
    }

    public function setProductId(string $productId) {
        $this->productId = $productId;
    }

    public function getQuantity() {
        return $this->quantity;
    }

    public function setQuantity(float $quantity) {
        $this->quantity = $quantity;
    }
    
    public function getUnitPrice() {
        return $this->unitPrice;
    }

    public function setUnitPrice(float $unitPrice) {
        $this->unitPrice = $unitPrice;
    }

    public function getTotal() {
        return $this->total;
    }

    public function setTotal(float $total) {
        $this->total = $total;
    }
}
