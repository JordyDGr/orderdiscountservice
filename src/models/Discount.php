<?php
namespace App\models;

class Discount
{

    public $type, $value;

    public function __construct(string $type, float $value){
        $this->type  = $type;
        $this->value = $value;
    }

    public function getType() {
        return $this->type;
    }

    public function getValue() {
        return $this->value;
    }
}