<?php
namespace App\models;

class Customer
{

    private $id, $name, $since, $revenue;

    public function getId() {
        return $this->id;
    }

    public function setId(int $id) {
        $this->id = $id;
    }

    public function getName() {
        return $this->name;
    }

    public function setName(string $name) {
        $this->name = $name;
    }
    
    public function getSince() {
        return $this->since;
    }

    public function setSince(string $since) {
        $this->since = $since;
    }

    public function getRevenue() {
        return $this->revenue;
    }

    public function setRevenue(float $revenue) {
        $this->revenue = $revenue;
    }
}
