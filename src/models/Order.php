<?php
namespace App\models;

class Order
{

    private $id, $customerId, $items, $total;

    public function getId() {
        return $this->id;
    }

    public function setId(int $id) {
        $this->id = $id;
    }

    public function getCustomerId() {
        return $this->customerId;
    }

    public function setCustomerId(int $customerId) {
        $this->customerId = $customerId;
    }

    public function getItems() {
        return $this->items;
    }

    public function setItems(array $items) {
        $this->items = $items;
    }

    public function getTotal() {
        return $this->total;
    }

    public function setTotal(float $total) {
        $this->total = $total;
    }

}
