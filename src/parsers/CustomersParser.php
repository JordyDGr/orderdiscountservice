<?php
namespace App\parsers;

use App\models\Customer;

class CustomersParser
{
    static function parse($data) {
        $customers = [];

        foreach ($data as $customer) {
            $newCust = new Customer();
            $newCust->setId( intval($customer->id) );
            $newCust->setName( $customer->name );
            $newCust->setSince( $customer->since );
            $newCust->setRevenue( $customer->revenue );
            $customers[] = $newCust;
        }

        return $customers;
    }
}

