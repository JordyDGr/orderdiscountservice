<?php
namespace App\parsers;

use App\models\Product;

class ProductsParser
{
    static function parse($data) {
        $products = [];
        
        foreach ($data as $prod) {
            $product = new Product();
            $product->setId( $prod->id );
            $product->setDescription( $prod->description );
            $product->setCategory( $prod->category );
            $product->setPrice( $prod->price );
            $products[] = $product;
        }
        return $products;
    }
}

    