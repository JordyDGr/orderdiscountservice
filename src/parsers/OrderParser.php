<?php
namespace App\parsers;

use App\models\Order;
use App\models\Item;

class OrderParser
{
    static function parse($order) {
        $newOrder = new Order();
        $newOrder->setId( $order['id'] );
        $newOrder->setCustomerId( $order['customer-id'] );

        $items = [];
        foreach ($order['items'] as $item) {
            $newItem = new Item();
            $newItem->setProductId( $item['product-id'] );
            $newItem->setQuantity( $item['quantity'] );
            $newItem->setUnitPrice( $item['unit-price'] );
            $newItem->setTotal( $item['total'] );
            $items[] = $newItem;
        }

        $newOrder->setItems( $items );
        $newOrder->setTotal( $order['total'] );

        return $newOrder;
    }
}

