<?php
include_once(__DIR__ . '/vendor/autoload.php');

use App\parsers\CustomersParser;
use App\parsers\OrderParser;
use App\parsers\ProductsParser;
use App\services\DiscountFacade;


// get products from 'API'
$rawProductsData  = json_decode(file_get_contents(__DIR__ . '/data/products.json'));
$products = ProductsParser::parse($rawProductsData);

// get customers from 'API'
$rawCustomersData  = json_decode(file_get_contents(__DIR__ . '/data/customers.json'));
$customers = CustomersParser::parse($rawCustomersData);

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $order     = OrderParser::parse( json_decode(file_get_contents('php://input'), true) );
    $discFacade  = new DiscountFacade($customers , $products);
    echo json_encode($discFacade->calculateDiscount($order));
}