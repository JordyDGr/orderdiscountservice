<?php
use PHPUnit\Framework\TestCase;
use App\parsers\ProductsParser;
use App\models\Product;

class ProductsParserTest extends TestCase
{
    private $productsParser;
    
    protected function setUp() {
        $this->productsParser = new ProductsParser();
        $this->data = json_decode(file_get_contents(dirname(__FILE__, 3). '/data/products.json'));
    }

    public function testObjectsAreProducts() {
        $products = $this->productsParser->parse($this->data);
        foreach ($products as $product) {
            $this->assertInstanceOf(Product::class, $product); 
        }
    }
    
}