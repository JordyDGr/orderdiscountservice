<?php
use PHPUnit\Framework\TestCase;
use App\parsers\OrderParser;
use App\models\Order;
use App\models\Item;

class OrderParserTest extends TestCase
{
    private $orderParser;
    
    protected function setUp() {
        $this->orderParser = new OrderParser();
        $this->data = json_decode(file_get_contents(dirname(__FILE__, 3). '/data/order1.json'), true);
    }

    public function testObjectisOrder() {
        $order = $this->orderParser->parse( $this->data );
        $this->assertInstanceOf(Order::class, $order);
    }

    public function testOrderItemsAreItems() {
        $order = $this->orderParser->parse( $this->data );
        foreach ($order->getItems() as $item) {
            $this->assertInstanceOf(Item::class, $item); 
        }
    }

    public function testOrderHasAtLeast1Item() {
        $order = $this->orderParser->parse( $this->data );
        $this->assertGreaterThanOrEqual(count($order->getItems()), 1);
    }
    
}