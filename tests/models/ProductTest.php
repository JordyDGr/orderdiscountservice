<?php
use PHPUnit\Framework\TestCase;

use App\models\Product;

class ProductTest extends TestCase
{    
    /**
     * @expectedException TypeError
     */
    public function testWrongTypeException(){
        $product = new Product();
        $product->setPrice("string instead of int");
    }

    public function testConvertNegativePrice(){
        $product = new Product();
        $product->setPrice(-10.00);
        $this->assertGreaterThan( 0 , $product->getPrice() );
    }
}