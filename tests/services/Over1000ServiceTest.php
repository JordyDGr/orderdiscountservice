<?php
use PHPUnit\Framework\TestCase;
use App\services\Over1000Service;
use App\models\Customer;
use App\models\Discount;
use App\models\Order;
use App\models\Item;

class Over1000ServiceTest extends TestCase
{
    private $order;
    
    protected function setUp() {
        $this->order = new Order();
        $this->order->setId(1);
        $this->order->setCustomerId(1);
        $this->order->setItems([new Item()]);
        $this->order->setTotal(123.40);
    }

    public function testReturnsDiscountObjectWhenValid() {
        $customer = new Customer();
        $customer->setId(1);
        $customer->setRevenue(1500.0);
        
        $service  = new Over1000Service([], [$customer] );
        $this->assertInstanceOf(Discount::class, $service->calculate($this->order) ); 
    }

    public function testReturnsNullWhenInvalid() {
        $customer = new Customer();
        $customer->setId(1);
        $customer->setRevenue(10.0);
        
        $service  = new Over1000Service([], [$customer] );
        $this->assertNull( $service->calculate($this->order) ); 
    }

    public function testRevenueUnder1000() {
        $customer = new Customer();
        $customer->setId(1);
        $customer->setRevenue(100.0);
        
        $service  = new Over1000Service([], [$customer] );
        $this->assertNull( $service->calculate($this->order) );
    }

    public function testRevenueExactly1000() {
        $customer = new Customer();
        $customer->setId(1);
        $customer->setRevenue(1000.00);
        
        $service  = new Over1000Service([], [$customer] );
        $this->assertNull( $service->calculate($this->order) );
    }

    public function testRevenueOver1000() {
        $customer = new Customer();
        $customer->setId(1);
        $customer->setRevenue(1000.01);
        
        $service  = new Over1000Service([], [$customer] );
        $discount = $service->calculate($this->order);
        $this->assertEquals( 12.34 , $discount->getValue() );
    }
}