<?php
use PHPUnit\Framework\TestCase;
use App\services\MoreThan1Cat1Service;
use App\models\Discount;
use App\models\Order;
use App\models\Product;
use App\models\Item;

class MoreThan1Cat1ServiceTest extends TestCase
{
    private $order;
    
    protected function setUp() {
        $this->order = new Order();
        $this->order->setId(1);
        $this->order->setCustomerId(1);
        $this->order->setTotal(123.40);
        $product1 = new Product();
        $product1->setId(1);
        $product1->setCategory(1);
        $product2 = new Product();
        $product2->setId(2);
        $product2->setCategory(1);
        $this->products = [$product1, $product2];
    }

    public function testReturnsDiscountObjectWhenValid() {
        $item1 = new Item();
        $item1->setProductId(1);
        $item1->setQuantity(10);
        $item1->setUnitPrice(2.00);
        $item1->setTotal(20.00);

        $this->order->setItems( [ $item1 ] );
        $service  = new MoreThan1Cat1Service($this->products);
        $this->assertInstanceOf(Discount::class, $service->calculate($this->order) ); 
    }

    public function testReturnsNullWhenInvalid() {
        $item1 = new Item();
        $item1->setProductId(1);
        $item1->setQuantity(1);
        $item1->setUnitPrice(2.00);
        $item1->setTotal(2.00);

        $this->order->setItems( [ $item1 ] );
        $service  = new MoreThan1Cat1Service( $this->products );
        $this->assertNull( $service->calculate($this->order) ); 
    }

    public function test1Item2Products() {
        $item1 = new Item();
        $item1->setProductId(1);
        $item1->setQuantity(2);
        $item1->setUnitPrice(2.00);
        $item1->setTotal(4.00);

        $this->order->setItems( [ $item1 ] );
        $service  = new MoreThan1Cat1Service($this->products);
        $discount = $service->calculate($this->order);
        $this->assertEquals( 0.80 , $discount->getValue() ); 
    }

    public function test2Items3Products() {
        $item1 = new Item();
        $item1->setProductId(1);
        $item1->setQuantity(1);
        $item1->setUnitPrice(3.00);
        $item2 = new Item();
        $item2->setProductId(1);
        $item2->setQuantity(2);
        $item2->setUnitPrice(2.00);

        $this->order->setItems( [ $item1, $item2] );
        $service  = new MoreThan1Cat1Service($this->products);
        $discount = $service->calculate($this->order);
        $this->assertEquals( 0.80 , $discount->getValue() ); 
    }    


}