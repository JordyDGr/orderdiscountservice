<?php
use PHPUnit\Framework\TestCase;
use App\services\BuyFiveOneFreeService;
use App\models\Discount;
use App\models\Order;
use App\models\Product;
use App\models\Item;

class BuyFiveOneFreeServiceTest extends TestCase
{
    private $order;
    
    protected function setUp() {
        $this->order = new Order();
        $this->order->setId(1);
        $this->order->setCustomerId(1);
        $this->order->setTotal(123.40);
        $product1 = new Product();
        $product1->setId(1);
        $product1->setCategory(2);
        $product2 = new Product();
        $product2->setId(2);
        $product2->setCategory(2);
        $this->products = [$product1, $product2];
    }

    public function testReturnsDiscountObjectWhenValid() {
        $item1 = new Item();
        $item1->setProductId(1);
        $item1->setQuantity(10);
        $item1->setUnitPrice(2.00);

        $this->order->setItems( [ $item1 ] );
        $service  = new BuyFiveOneFreeService($this->products);

        $this->assertInstanceOf(Discount::class, $service->calculate($this->order) ); 
    }

    public function testReturnsNullWhenInvalid() {
        $item1 = new Item();
        $item1->setProductId(1);
        $item1->setQuantity(2);
        $item1->setUnitPrice(2.00);
        $item1->setTotal(4);

        $this->order->setItems( [ $item1 ] );
        $service  = new BuyFiveOneFreeService( $this->products );

        $this->assertNull( $service->calculate($this->order) ); 
    }

    public function testJust5Items() {
        $item1 = new Item();
        $item1->setProductId(1);
        $item1->setQuantity(5);
        $item1->setUnitPrice(2.00);

        $this->order->setItems( [ $item1 ] );
        $service  = new BuyFiveOneFreeService($this->products);
        $this->assertNull( $service->calculate($this->order ));
    }

    public function testExactly6Items() {
        $item1 = new Item();
        $item1->setProductId(1);
        $item1->setQuantity(6);
        $item1->setUnitPrice(2.00);

        $this->order->setItems( [ $item1 ] );
        $service  = new BuyFiveOneFreeService($this->products);
        $discount = $service->calculate($this->order);
        $this->assertEquals( 2.00 , $discount->getValue() ); 
    }

    public function testMoreThan6Items() {
        $item1 = new Item();
        $item1->setProductId(1);
        $item1->setQuantity(19);
        $item1->setUnitPrice(2.00);

        $this->order->setItems( [ $item1 ] );
        $service  = new BuyFiveOneFreeService($this->products);
        $discount = $service->calculate($this->order);
        $this->assertEquals( 6.00 , $discount->getValue() ); 
    }

    public function testMoreThen6ItemsCombined() {
        $item1 = new Item();
        $item1->setProductId(1);
        $item1->setQuantity(5);
        $item1->setUnitPrice(2.00);

        $item2 = new Item();
        $item2->setProductId(2);
        $item2->setQuantity(5);
        $item2->setUnitPrice(2.00);

        $this->order->setItems( [ $item1, $item2 ] );
        $service  = new BuyFiveOneFreeService($this->products);
        $this->assertNull( $service->calculate($this->order ));
    }


}