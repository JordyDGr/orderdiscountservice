# **Order discount Mini-service**

a small PHP (micro)service that calculates discounts for orders.

### Installation
Run `composer install` to install the required dependencies.

### Run miniservice
Do a HTTP POST request with a (json string containing the order) to the root

### Unit Tests
There are PHPUnit-tests available for (some) models, services and parsers.
Run `vendor/bin/phpunit tests` in terminal/command line to see testing results